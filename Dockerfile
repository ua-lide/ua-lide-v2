FROM php:7.2-fpm

# Copie du fichier resolv.conf
COPY ./resolv.conf /resolv.conf

WORKDIR /app

# Installation de Composer
# https://getcomposer.org/download/
RUN cat /resolv.conf > /etc/resolv.conf &&\
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" &&\
	php -r "if (hash_file('sha384', 'composer-setup.php') === 'c5b9b6d368201a9db6f74e2611495f369991b72d9c8cbd3ffbc63edff210eb73d46ffbfce88669ad33695ef77dc76976') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" &&\
	php composer-setup.php &&\
	php -r "unlink('composer-setup.php');"

# Installation du Driver pdo_mysql + autres extensions nécessaires (ZIP)
RUN cat /resolv.conf > /etc/resolv.conf && \
	apt-get update && apt-get install -y zlib1g-dev libzip-dev &&\
	docker-php-ext-install pdo_mysql zip
	
RUN mv composer.phar /usr/bin/composer

# Installation de NodeJS et NPM + GIT
# https://medium.com/@sirajul.anik/node-js-in-a-docker-php-container-40882a036e71
RUN cat /resolv.conf > /etc/resolv.conf &&\
	apt-get update -y &&\
	apt-get install -y gnupg2 &&\
	rm -rf /var/lib/apt/lists/ &&\
	cd / &&\
	curl https://deb.nodesource.com/setup_12.x | bash - &&\
	apt-get install -y nodejs git

EXPOSE 8000