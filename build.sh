#!/bin/bash

WEB_PHP_IMAGE_NAME=lide_php
WEB_PHP_CONTAINER_NAME=lide_php

SRC_DIR=lide_web

# Build
docker image build -t $WEB_PHP_IMAGE_NAME .

# Run (/bin/bash)
docker run -it --rm --name $WEB_PHP_CONTAINER_NAME -v $PWD/$SRC_DIR:/app -p 8000:8000 $WEB_PHP_IMAGE_NAME /bin/bash
