<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LangageRepository")
 */
class Langage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $options;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $compilateur;

    /**
     * @ORM\Column(type="text")
     */
    private $dockerfile;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $dockerName;

    /**
     * @ORM\Column(type="text")
     */
    private $script;

    /**
     * @ORM\Column(type="boolean")
     */
    private $actif;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getOptions(): ?string
    {
        return $this->options;
    }

    public function setOptions(?string $options): self
    {
        $this->options = $options;

        return $this;
    }

    public function getCompilateur(): ?string
    {
        return $this->compilateur;
    }

    public function setCompilateur(string $compilateur): self
    {
        $this->compilateur = $compilateur;

        return $this;
    }

    public function getDockerfile(): ?string
    {
        return $this->dockerfile;
    }

    public function setDockerfile(string $dockerfile): self
    {
        $this->dockerfile = $dockerfile;

        return $this;
    }

    public function getDockerName(): ?string
    {
        return $this->dockerName;
    }

    public function setDockerName(string $dockerName): self
    {
        $this->dockerName = $dockerName;

        return $this;
    }

    public function getScript(): ?string
    {
        return $this->script;
    }

    public function setScript(string $script): self
    {
        $this->script = $script;

        return $this;
    }

    public function getActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }
}
