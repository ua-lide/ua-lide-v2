#!/bin/bash

echo -e "\033[1;34m [LIDE] Installation des dépendances PHP avec Composer\033[0m"
composer install -n --no-progress --no-suggest

echo -e "\033[1;34m [LIDE] Installation de toutes les dépendances Node\033[0m"
npm install

echo -e "\033[1;34m [LIDE] Compilation des sources VueJS\033[0m"
./node_modules/.bin/encore dev