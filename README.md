# Projet LIDE

Le but de ce projet est de créer un environnement de développement en ligne dédié aux étudiants de licence.
Cette application leur permettra d’éviter le téléchargement de tous les éditeurs de code (complexes) nécessaires dans le cadre des `Cours`, `TP` et `Projets`.

L'interface devra être sobre et plus simple pour ne pas déstabiliser les étudiants débutants (par exemple ceux de la 1<sup>ère</sup> année de licence).

## Technologies

Ce projet est développé principalement autour des frameworks :
- [Symfony](https://symfony.com/) en back-end;
- [VueJS](https://vuejs.org/) en front-end.

Bases de données : [MySQL](https://www.mysql.com/fr/).

L'application `LIDE`, dans sa globalité, est déployé dans des conteneurs [Docker](https://www.docker.com/).

## Symfony + VueJS

[Symfony + vuejs](https://blog.dev-web.io/2018/01/11/symfony-4-utiliser-vue-js/)