#!/bin/bash

docker-compose up -d --build

echo -e "\033[1;32m*********************************************************\033[0m"
echo -e "\033[1;32m *** Exécution des instructions du script ./build.sh *** \033[0m"
echo -e "\033[1;32m*********************************************************\033[0m"
docker-compose exec web ./build.sh

echo -e "\033[1;32m****************************\033[0m"
echo -e "\033[1;32m*** Lancement du serveur ***\033[0m"
echo -e "\033[1;32m****************************\033[0m"
docker-compose exec -d web php bin/console server:run 0.0.0.0:8000